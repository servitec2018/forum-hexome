<?php



if (!defined('IN_MYBB')) {
    die('(-_*) This file cannot be accessed directly.');
}

mb_internal_encoding("UTF-8");

define('ROOT', str_replace("\\", "/", MYBB_ROOT));
define('PLUG_ROOT', ROOT . 'inc/plugins/');

global $templatelist;

if (isset($templatelist)) {
    $templatelist .= ',';
}

if (defined('THIS_SCRIPT')) {
	if (THIS_SCRIPT == 'index.php') {
        $templatelist .= 'index_main_advertisement,index_panel_advertisement,';
        $templatelist .= 'index_panel_trends,index_panel_trends_bit,index_panel_trends_last_poster,index_panel_trends_error,';
        $templatelist .= 'index_panel_last_registered';
	}
}

if (isset($templatelist)) {
    $templatelist .= ',';
}

$templatelist .= 'header_quest_block,header_advertisement';

if (!defined('IN_ADMINCP')) {
    $plugins->add_hook('global_intermediate', 'gray_main');
    $plugins->add_hook('global_intermediate', 'gray_header_quest_block');
    $plugins->add_hook('global_intermediate', 'gray_header_advertisement');

    if (THIS_SCRIPT == 'index.php') {
        $plugins->add_hook('index_start', 'gray_index_main_advertisement');
        $plugins->add_hook('index_start', 'gray_index_panel_advertisement');
        $plugins->add_hook('index_start', 'gray_index_panel_trends');
        $plugins->add_hook('index_start', 'gray_index_panel_last_registered');
    }

    if (THIS_SCRIPT == 'forumdisplay.php') {
        $plugins->add_hook("forumdisplay_thread", "gray_forumdisplay_thread");
        $plugins->add_hook("forumdisplay_announcement", "gray_forumdisplay_announcement");
    }
}

function gray_info() {
    return array(
        'name'          => 'Gray',
        'description'   => 'Gray temasının özelliklerini çalıştırır.',
        'website'       => '',
        'author'        => 'Sgc',
        'authorsite'    => $url = 'https://coldfrm.com/uye-sgc.html',
        'version'       => '1.2.0',
        'codename'      => 'Sgc_gray',
        'compatibility' => '18*'
    );
}

function gray_install() {
    global $db, $cache;

    $info = gray_info();

    $Sgc = $cache->read('Sgc');
    $Sgc[$info['codename']] = [
        'name'      => $info['name'],
        'author'    => $info['author'],
        'version'   => $info['version'],
    ];
    $cache->update('Sgc', $Sgc);
}

function gray_is_installed() {
    global $cache;

    $info = gray_info();

    $installed = $cache->read('Sgc');
    if ($installed[$info['codename']]) {
        return true;
    }
}

function gray_uninstall() {
    global $db, $cache;

    $info = gray_info();

    $Sgc = $cache->read('Sgc');
    unset($Sgc[$info['codename']]);
    $cache->update('Sgc', $Sgc);

    if (count($Sgc) == 0) {
        $db->delete_query('datacache', "title='Sgc'");
    }
}

function gray_activate() {}

function gray_deactivate() {}

function gray_main() {
    global $mybb, $db, $cache, $theme, $templates;
    global $gray;

    $info = gray_info();

    $gray = [
        'auth' => [
            'nick'      => gray_nick($mybb->user['username'], $mybb->user['usergroup'], $mybb->user['displaygroup']),
            'avatar'    => gray_avatar($mybb->user['avatar']),
        ],
        'site' => [
            'name'  => gray_site_name(),
            'short' => gray_site_short_name(),
        ],
        'skin' => [
            'name'          => $info['name'],
            'author'        => $info['author'],
            'author_link'   => $info['authorsite'],
            'version'       => $info['version'],
        ],
    ];
}

function gray_header_quest_block() {
    global $mybb;
    if ($mybb->user['uid'] == 0) {
        global $templates, $header_quest_block;
        $header_quest_block = eval($templates->render('header_quest_block'));
    }
}

function gray_header_advertisement() {
    global $templates, $header_advertisement;
    $header_advertisement = eval($templates->render('header_advertisement'));
}

function gray_index_main_advertisement() {
    global $templates, $index_main_advertisement;
    $index_main_advertisement = eval($templates->render('index_main_advertisement'));
}

function gray_index_panel_advertisement() {
    global $templates, $index_panel_advertisement;
    $index_panel_advertisement = eval($templates->render('index_panel_advertisement'));
}

function gray_index_panel_trends() {
    global $db, $theme, $templates, $index_panel_trends;

    $trends = $db->simple_select("threads", "tid, uid, subject, lastposteruid, views", "visible = 1", array(
        "order_by"  => 'views',
        "order_dir" => 'DESC',
        "limit"     => 5,
    ));

    $index_panel_trends_last_poster = "";

    if ($db->num_rows($trends) > 0) {
        while ($trend = $db->fetch_array($trends)) {
            $trend['subject'] = htmlspecialchars_uni($trend['subject']);
            $trend['link'] = get_thread_link($trend['tid']);
            $trend['views'] = number_format($trend['views']);

            // Author
            $author = $db->simple_select("users", "username, usertitle, avatar, usergroup, displaygroup", "uid = '{$trend['uid']}'");
            $author = $db->fetch_array($author);

            $author['title'] = gray_user_title($author['usertitle'], $author['usergroup']);
            $author['username'] = htmlspecialchars_uni($author['username']);
            $author['formatted'] = gray_nick($author['username'], $author['usergroup'], $author['displaygroup']);
            $author['profile'] = get_profile_link($trend['uid']);
            $author['avatar'] = gray_avatar($author['avatar']);

            // Last Poster
            $index_panel_trends_last_poster = '';
            if ($trend['uid'] != $trend['lastposteruid']) {
                $last_poster = $db->simple_select("users", "username, usertitle, avatar, usergroup", "uid = '{$trend['lastposteruid']}'");
                $last_poster = $db->fetch_array($last_poster);

                $last_poster['title'] = gray_user_title($last_poster['usertitle'], $last_poster['usergroup']);
                $last_poster['nick'] = htmlspecialchars_uni($last_poster['username']);
                $last_poster['avatar'] = gray_avatar($last_poster['avatar']);
                $last_poster['link'] = get_thread_link($trend['tid'], 0, 'lastpost');

                $index_panel_trends_last_poster = eval($templates->render('index_panel_trends_last_poster'));
            }

            $index_panel_trends_bit .= eval($templates->render('index_panel_trends_bit'));
        }
    } else {
        $index_panel_trends_error = eval($templates->render('index_panel_trends_error'));
    }

    $trend['num'] = $db->num_rows($trends);

    $index_panel_trends = eval($templates->render('index_panel_trends'));
}

function gray_index_panel_last_registered() {
    global $db, $cache, $templates, $index_panel_last_registered;
    
    $stats = $cache->read('stats');
    $uid = $stats['lastuid'];

    $users = $db->simple_select("users", "username, avatar, usergroup, displaygroup, regdate", "uid = '{$uid}'");
    $user = $db->fetch_array($users);

    $user['nick'] = gray_escape($user['username']);
    $user['nick'] = gray_nick($user['nick'], $user['usergroup'], $user['displaygroup']);
    $user['profile'] = get_profile_link($uid);
    $user['avatar'] = gray_avatar($user['avatar']);
    $user['register'] = my_date('relative', $user['regdate']);

    $index_panel_last_registered = eval($templates->render('index_panel_last_registered'));
}

function gray_forumdisplay_thread() {
    global $mybb, $db, $thread, $author;

    $query = $db->simple_select("users", "username, avatar, usergroup, displaygroup", "uid = '{$thread['uid']}'");
    $author = $db->fetch_array($query);

    $author['nick'] = gray_escape($author['username']);
    $author['avatar'] = gray_avatar($author['avatar']);
    $author['display'] = gray_nick($author['nick'], $author['usergroup'], $author['displaygroup']);
    $author['profile'] = get_profile_link($thread['uid']);
}

function gray_forumdisplay_announcement() {
    global $mybb, $db, $announcement, $author;

    $query = $db->simple_select("users", "username, avatar, usergroup, displaygroup", "uid = '{$announcement['uid']}'");
    $author = $db->fetch_array($query);

    $author['nick'] = gray_escape($author['username']);
    $author['avatar'] = gray_avatar($author['avatar']);
    $author['display'] = gray_nick($author['nick'], $author['usergroup'], $author['displaygroup']);
    $author['profile'] = get_profile_link($announcement['uid']);
}

function gray_site_name() {
    $name = gray_escape(strtolower($_SERVER['SERVER_NAME']));
    $name = ucwords($name);
    return $name;
}

function gray_site_short_name() {
    $name = gray_site_name();
    $name = mb_substr($name, 0, 1) . '.';
    return $name;
}

function gray_nick($nick, $id, $group) {
    if (isset($nick) && isset($id) && isset($group)) {
        if (function_exists('format_name')) {
            return format_name(gray_escape($nick), $id, $group);
        } else {
            return gray_escape($nick);
        }
    }
}

function gray_avatar($avatar) {
    if (!empty($avatar) && is_string($avatar)) {
        return $avatar;
    } else {
        return gray_asset_url() . '/assets/images/default-avatar.png';
    }
}

function gray_user_title($title, $group) {
    if (!empty($title)) {
        $title = $title;
    } else {
        $group = usergroup_displaygroup($group);
        $title = (!empty($group['usertitle'])) ? $group['usertitle'] : $group['title'];
    }

    return htmlspecialchars_uni($title);
}

function gray_asset_url() {
    global $mybb;
    return $mybb->asset_url . '/' . gray_code_name();
}

function gray_escape($var) {
    return htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
}

function gray_code_name() {
    $info = gray_info();
    $name = str_replace('tedem_', '', $info['codename']);
    return $name;
}
