<?php
// Show First Post plugin
// By Jasiu
// Fixed for 1.4.x by krig
// Fixed for 1.8.x by rockenren
// Fixed for PHP 7.2 by panayot
if (!defined("IN_MYBB")) {
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

if(!defined("SHOW_FIRST_POST_LANG_FILE")) {
	define("SHOW_FIRST_POST_LANG_FILE", MYBB_ROOT."inc/languages/english/show_first_post.lang.php");
}

if (!function_exists('q')) {
	function q($str) {
		return "#" . preg_quote($str) . "#";
	}
}
$plugins->add_hook("showthread_start", "show_first_post_init_lang");
$plugins->add_hook("showthread_linear", "show_first_post");
$plugins->add_hook("moderation_start", "moderation_hook");
$plugins->add_hook("moderation_stick", "moderation_stick");

function show_first_post_info() {
	return array(
		"name"			=> "Show First Post",
		"description"	=> "Shows first post of the thread in second and next pages",
		"website"		=> "https://community.mybb.com/thread-36388.html",
		"author"		=> "jasiu, dkrig, rockenren, panayot",
		"authorsite"	=> "",
		"version"		=> "1.8",
		"compatibility" => "18*",
		"codename"		=> "show_first_post"
	);
}

function check_lang_file() {
	if(!file_exists(SHOW_FIRST_POST_LANG_FILE))
    {
        flash_message("The selected plugin could not be installed because " . SHOW_FIRST_POST_LANG_FILE . " is missing.", "error");
        admin_redirect("index.php?module=config-plugins");
	}
}

function show_first_post_install() {
	global $db, $mybb;
	
	check_lang_file();
	
	$setting_group = array (
		"gid" => "NULL",
		"name" => "first_post_mod_settings",
		"title" => "Show First Post Plugin Settings",
		"description" => "Settings for the Show First Post plugin.",
		"disporder" => "4",
		"isdefault" => "no",

	);
	$db->delete_query('settinggroups', "name = 'first_post_mod_settings'");
	$gid = $db->insert_query("settinggroups", $setting_group);

	$setting_1 = array (
		"sid" => "NULL",
		"name" => "auto_pin_forums",
		"title" => "Auto Pin First Post",
		"description" => "Auto Pin First Post In Forums(Example:1,72,12)",
		"optionscode" => "text",
		"value" => "",
		"disporder" => "2",
		"gid" => intval($gid),
	);
	$setting_2 = array (
		"sid" => "NULL",
		"name" => "auto_pin_all_forums",
		"title" => "Auto Pin First Post In All Forums",
		"description" => "",
		"optionscode" => "onoff",
		"value" => "",
		"disporder" => "1",
		"gid" => intval($gid),
	);

	$db->delete_query('settings', "name IN ('auto_pin_forums','auto_pin_all_forums')");
	
	$db->insert_query("settings", $setting_1);
	$db->insert_query("settings", $setting_2);
	
	rebuild_settings();


	if(!$db->field_exists("fix_first_post", "threads"))
		$db->add_column("threads", "fix_first_post", "TINYINT(1) UNSIGNED DEFAULT 0 NOT NULL");

}

function show_first_post_activate() {
	require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    global $db, $mybb;
    
    check_lang_file();
    
    // This is a fix for people upgrading from below 1.6 where settings were created in _activate() instead of _install()
    $query = $db->simple_select("settinggroups", "gid", "name='first_post_mod_settings'"); 
	if($db->num_rows($query) < 1)
	{ 	 
    	show_first_post_install();
    }
        

	$template_text = "<table border=\"0\" cellspacing=\"{\$theme['borderwidth']}\" cellpadding=\"{\$theme['tablespace']}\" class=\"tborder tfixed clear\">
				<tr>
				<td class=\"thead\">
				<div><strong>{\$lang->first_post} - {\$thread['threadprefix']}{\$thread['subject']}</strong></div>
				</td>
				</tr>
				<tr>
				<td id=\"posts_container\">
				<div id=\"posts\">
				{\$posts}
				</div>
				</td>
				</tr>
				</table>
				<br />";

	$fp_template = array (
		"title" => "global_first_post",
		"template" => $db->escape_string($template_text),
		"sid" => -1,
		"version" => 122,
		"status" => "",
		"dateline" => time(),);
	$db->insert_query('templates', $fp_template);
	find_replace_templatesets("showthread", '#{\$header}(\r?)\n#', "{\$header}\n{\$fpost2}\n");
	find_replace_templatesets("showthread_moderationoptions_stickunstick", q('{$lang->stick_unstick_thread}</option>'), '{$lang->stick_unstick_thread}</option><option class="option_mirage" value="fix">{$lang->fix_first_post}</option>');

}
function show_first_post_deactivate() {
	require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
	global $db;

	$db->delete_query('templates', "title = 'global_first_post'");
	find_replace_templatesets("showthread", '#{\$fpost2}(\r?)\n#', "", 0);
	// older versions of plugin did not have class="option_mirage" so we need to remove both variants
	find_replace_templatesets("showthread_moderationoptions_stickunstick", '#' . preg_quote('<option ') . '(class="option_mirage" )?' . preg_quote('value="fix">{$lang->fix_first_post}</option>') . '#', "", 0);

}
function do_show_first_post() {
	global $db, $mybb, $templates, $fpost2, $firstpostcontent, $thread, $postcounter, $page, $lang, $settings, $attachcache;
	$forums = explode(",",$settings['auto_pin_forums']);
	if (!function_exists('build_postbit'))
	{
		require_once MYBB_ROOT . '/inc/functions_post.php';
	}
	if ($thread['fix_first_post'] > 0 || $settings['auto_pin_all_forums'] || array_search($thread['fid'],$forums)!==false) {
		$pfirst = true;
		$posts = '';
		$sql = "SELECT u.*, u.username " .
		"AS userusername, p.*, f.*, eu.username AS editusername" .
		" FROM " . TABLE_PREFIX . "posts p" .
		" LEFT JOIN " . TABLE_PREFIX . "users u ON ( u.uid = p.uid ) " .
		" LEFT JOIN " . TABLE_PREFIX . "userfields f ON( f.ufid = u.uid ) " .
		" LEFT JOIN " . TABLE_PREFIX . "users eu ON( eu.uid = p.edituid ) " .
		" WHERE p.tid = '" . $thread['tid'] . "' " .
		" ORDER BY p.dateline ASC LIMIT 0, 1 ";
		$query = $db->query($sql);
		
		$post = $db->fetch_array($query);
		if(!empty($post))
		{
			$query = $db->simple_select("attachments", "*", "pid=".$post['pid']);
			while($attachment = $db->fetch_array($query))
			{
				$attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
			}
			$post['visible'] = 1;
			
			// need to change global $postcounter and $page before build_postbit() so that the number of the post is always 1
			// after we build the post bit, we restore their original values
			$originalpostcounter = $postcounter;
			$originalpage = $page;
			$postcounter = 0;
			$page = 1;
			
			$posts .= build_postbit($post);
			
			$postcounter = $originalpostcounter;  
        	$page = $originalpage;
			
			$post = '';
			$pfirst = false;
		
			eval (" \$fpost2 = \"" . $templates->get("global_first_post") . " \";");
		}
	}
}

function show_first_post_uninstall()
{
	global $mybb, $db;
	
	if($mybb->request_method != 'post')
	{
		global $page, $lang;
		
		//TODO: add inc/languages/english/admin/config_show_first_post.php
		//$lang->load('config_show_first_post');
		//$page->output_confirm_action('index.php?module=config-plugins&action=deactivate&uninstall=1&plugin=show_first_post', $lang->sfp_uninstall_message, $lang->sfp_uninstall);
		
		$page->output_confirm_action('index.php?module=config-plugins&action=deactivate&uninstall=1&plugin=show_first_post', "Each thread has a setting whether first post is pinned. Do you wish to remove all these settings?", "Uninstall First Post MOD");
	}
	
	if(!isset($mybb->input['no']))
	{		
		$db->delete_query('settings', "name IN ('auto_pin_forums','auto_pin_all_forums')");
		$db->delete_query('settinggroups', "name = 'first_post_mod_settings'");

		rebuild_settings();
		
		if($db->field_exists('fix_first_post', 'threads'))
		{
			$db->drop_column("threads", "fix_first_post"); 
		}
	}
}

function show_first_post_is_installed()
{
	global $db;	
	
	if($db->field_exists('fix_first_post', 'threads'))
	{
		return true;
	}
	return false;
}

function show_first_post_init_lang() {
	global $lang;
	$lang->load("show_first_post");
}

function show_first_post() {
	global $start, $fpost2;
	if ($start == 0) {
		$fpost2 = "";
	} else {
		do_show_first_post();
	}
}
function moderation_hook() {
	global $fixFirstPost, $mybb;
	if ($mybb->input['action'] == "fix") {
		$mybb->input['action'] = 'stick';
		$fixFirstPost = true;
	} else {
		$fixFirstPost = false;
	}
}
function moderation_stick() {
	global $fixFirstPost, $tid, $thread, $db;
	if ($fixFirstPost) {
		$fix = array (
			"fix_first_post" => $thread['fix_first_post'] ? 0 : 1,
		);
		$db->update_query("threads", $fix, "tid=$tid");
		redirect("showthread.php?tid=$tid", "");
	}
}


?>
